# paramiko package is required for connecting to a api_server through ssh
import paramiko
import re
from scp import SCPClient, SCPException

from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidNodeException, BadHostkeyException, UnexpectedNodeException
from xpresso.ai.core.logging.xpr_log import XprLogger

config_path = XprConfigParser.DEFAULT_CONFIG_PATH
config = XprConfigParser(config_path)

logger = XprLogger()


class SSHUtils:
    def __init__(self, server):
        self.server = server
        self.client = None
        self.connect()

    def connect_using_ssh_key(self, username, key_file):
        """
        This method helps to establish SSH connection with host using key file
        Args:
            username: name of user
            key_file: SSH public Id key file for a user
        Returns:
            [SSHClient object] self.client: paramiko SSHClient else False upon failure
        """
        try:
            self.client = paramiko.SSHClient()
            # In case host key is missing in known_hosts
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.client.connect(self.server, username=username, key_filename=key_file)
            return self.client
        except FileNotFoundError as exc:
            logger.debug(f"Server Authentication failed using key: \n {exc}")
            logger.debug(f"Missing SSH key file")
            return False
        except paramiko.AuthenticationException as exc:
            logger.debug(f"Server Authentication failed using key: \n {exc}")
            logger.debug(f"username : {config['vms']['username']}")
            return False

    def connect_using_password(self, username, password):
        """
        This method helps to establish ssh connection with host using password
        Args:
            username: Username of target server
            password: Password of VM
        Returns:
            [SSHClient object] self.cleint: paramiko SSH client
        """
        try:
            self.client = paramiko.SSHClient()
            self.client.connect(
                self.server,
                username=username,
                password=password
            )
            return self.client
        except paramiko.AuthenticationException as exc:
            logger.debug(f"Server Authentication failed using password: \n {exc}")
            logger.debug(f"username : {config['vms']['username']}")
            raise paramiko.AuthenticationException

    def connect(self):
        """
            creates a connection to the api_server through ssh

            Parameters:
                api_server [str]: ip_address of the api_server to connect to

            Return:
                [SSHClient object]: returns SSHClient instance
        """
        try:
            # SSHClient class represents a session with ssh api_server
            username = config['vms']['username']
            # ssh_key - path to .pem file
            key_file = config['vms'].get('ssh_key', '')
            if self.connect_using_ssh_key(username=username, key_file=key_file):
                return self.client
            else:
                password = config['vms']['password']
                self.connect_using_password(username=username, password=password)
                return self.client
        except paramiko.AuthenticationException as exc:
            logger.debug(f"Server Authentication failed using ssh key as well as password: \n {exc}")
            logger.debug(f"username : {config['vms']['username']}")
            raise InvalidNodeException
        except (paramiko.BadHostKeyException, paramiko.SSHException) as exc:
            # checks if the connection to the api_server succeeds
            # if connection is made then host key dict is updated
            logger.debug("Server connection failed : \n ", exc)
            raise BadHostkeyException
        except Exception as e:
            logger.debug("\n \n Error is : ", e)
            raise UnexpectedNodeException

    def preemptive_exec_with_regex_match(self, stdout_command: str,
                                         regex_match: str,
                                         stderr_command: str):
        """
        Executes the command and preemptively exits when the regex condition
        matches any of the output strings. Automatically exits when executed
        is completed. It picks the data

        Creates a new channel over the SSHClient connection
        then executes the command over this channel

        Parameters:
            stdout_command(str) : command to be executed on api_server to read stdout
            stderr_command(str) : command to be executed on api_server to read stderr
            regex_match(str): regex string which matches the command

        Return:
            dict: (status, stdout, stderr, result_match)
        """

        status = 1
        stdout, stderr = "", ""
        matched_result = ""
        try:
            status, matched_result, stdout = self.perform_regex_match(
                stdout_command, regex_match, is_err=False)
        except paramiko.SSHException as e:
            logger.exception(e)
        if matched_result:
            return {'status': 0, 'stdout': stdout, 'stderr': "",
                    "result_match": matched_result}
        try:
            status, matched_result, stderr = self.perform_regex_match(
                stderr_command, regex_match, is_err=True)
        except paramiko.SSHException as e:
            logger.exception(e)
        return {'status': status, 'stdout': stdout,
                'stderr': stderr, "result_match": matched_result}

    def perform_regex_match(self, command, regex_match, is_err):
        """
        Perform the regex match on the channel
        Args:
            command(str): command to run
            regex_match(str): regex string
            is_err(bool): check if is_err channel is required

        Returns:
            tuple: matched_result and stdout
        """
        buffer_byte = 128
        stdout = [""]
        matched_result = ""
        # creates a new tunnel over connection to ssh api_server
        channel = self.client.get_transport().open_session(timeout=60)

        # executes the command over the channel
        logger.debug(f"Running command: {command}")
        channel.exec_command(command)
        while not channel.exit_status_ready():
            if is_err:
                temp_out = channel.recv_stderr(buffer_byte).decode('utf-8')
            else:
                temp_out = channel.recv(buffer_byte).decode('utf-8')
            temp_out = temp_out.split('\n')
            print(temp_out)
            stdout[-1] += temp_out[0]
            stdout.extend(temp_out[1:])
            if len(stdout) < 2:
                continue
            current_stdout = stdout[-2]
            grouped_match = re.search(regex_match, current_stdout)
            if grouped_match:
                matched_result = grouped_match[1]
                break
        grouped_match = re.search(regex_match, stdout[-1])
        if grouped_match:
            matched_result = grouped_match[1]
        logger.debug("Matched Result " + matched_result)
        return 1, matched_result, '\n'.join(stdout)

    def exec(self, command):
        """
        executes a command on api_server through channel of SSHClient

        creates a new channel over the SSHClient connection
        then executes the command over this channel

        Parameters:
            command [str] : command to be executed on api_server

        Return:
            returns stdin, stdout, stderr of command execution process
        """
        # creates a new tunnel over connection to ssh api_server
        channel = self.client.get_transport().open_session()

        # executes the command over the channel
        channel.exec_command(command)
        # waits till the process executing the command is complete
        stdout = ''
        status = ''
        while not channel.exit_status_ready():
            temp_out = channel.recv(4294967296).decode('utf-8')
            if not temp_out.strip('\t\n '):
                logger.debug(temp_out.strip('\t\n '))
            stdout = stdout + temp_out

        stderr = channel.recv_stderr(4294967296).decode('utf-8')
        status = channel.recv_exit_status()
        logger.debug("status is ", status)
        return {'status': status, 'stdout': stdout, 'stderr': stderr}

    def exec_client(self, **kwargs):

        stdin_values = list()
        command = ""
        for key, value in kwargs.items():
            if key == "command":
                command = value
            else:
                stdin_values.append(value)
        try:
            stdin, stdout, stderr = self.client.exec_command(command)

            for val in stdin_values:
                stdin.write(val)

            logger.debug("exiting exec_client")
            return (stdin, stdout, stderr)
        except:
            return (None, None, None)

    def copy2server(self, local_path, remote_path, recursive=False, preserve_times=False):
        """
            copy files/directories to remote server

            Parameters:
                local_path [str]: source location on local to dir/file
                remote_path [str]: destination location on remote server
                recursive [bool]: transfer files and directories recursively
                preserve_times [bool]: preserve mtime and atime of transferred files/dir

        """
        try:
            scp_client = SCPClient(self.client.get_transport())
            scp_client.put(local_path, remote_path=remote_path, recursive=recursive, preserve_times=preserve_times)
        except SCPException as exc:
            logger.debug("SCP failed : \n ", exc)
            raise BadHostkeyException
        except Exception as e:
            logger.debug("\n \n Error is : ", e)
            raise UnexpectedNodeException

    def close(self):
        self.client.close()